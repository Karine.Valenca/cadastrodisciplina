import java.util.ArrayList;
public class ControleDisciplina{
	private ArrayList<Disciplina> listaDisciplinas;
//construtor
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();	
	}
//metodos
	public void adicionarDisciplina(Disciplina umaDisciplina){
		listaDisciplinas.add(umaDisciplina);		
		System.out.println("Disciplina adicionada com sucesso");
	}

	public void adicionarAlunoDisciplina(Aluno umAluno, Disciplina umaDisciplina) {
		umaDisciplina.adicionarAlunoDisciplina(umAluno);
		System.out.println("Aluno adicionado à disciplina com sucesso");
        }

	public void exibirDisciplinas () {
	        for (Disciplina umaDisciplina : listaDisciplinas) {
         		System.out.println(umaDisciplina.getNomeDisciplina());
	        }
	}	

	public Disciplina pesquisarDisciplina(String umNomeDisciplina){
		for (Disciplina umaDisciplina : listaDisciplinas) {
        		if (umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNomeDisciplina)) return umaDisciplina;
        	}
        return null;
	}
}
