import java.util.ArrayList;
public class Disciplina{
	private String nomeDisciplina;
	private ArrayList<Aluno> listaAlunosDisciplina;
	private String codigo;

//Construtor
	public void Disciplina(String umNomeDisciplina){
		nomeDisciplina = umNomeDisciplina;
	}
	
	public void Disciplina(){ 
		listaAlunosDisciplina = new ArrayList<Aluno>(); 
	}
//Métodos	
	public void setNomeDisciplina(String umNomeDisciplina){
		nomeDisciplina = umNomeDisciplina;
	}

	public String getNomeDisciplina(){
		return nomeDisciplina;
	}

	public void setCodigo(String umCodigo){
		codigo = umCodigo;
	}

	public String getCodigo(){
		return codigo;
	}

	public void adicionarAlunoDisciplina(Aluno umAluno){
		listaAlunosDisciplina.add(umAluno);
		
	}
}
