import java.util.ArrayList;

public class ControleAluno{

	private ArrayList <Aluno> listaAlunos;

//construtor
	public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();	
	}

//métodos
	public void adicionar(Aluno umAluno){
		listaAlunos.add(umAluno);		
		System.out.println("Aluno adicionado com sucesso");
	}

	public Aluno pesquisarAluno(String umNome){
		for (Aluno umAluno: listaAlunos) {
        		if (umAluno.getNome().equalsIgnoreCase(umNome)) return umAluno;
        }
        return null;
	}
	

	public void exibirAlunos () {
	        for (Aluno umAluno : listaAlunos) {
            System.out.println(umAluno.getNome());
	        }
	}	




}
