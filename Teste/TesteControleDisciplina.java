package Teste;


import Model.Disciplina;
import Model.ControleDisciplina;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TesteControleDisciplina {
	private ControleDisciplina umControleDisciplina;
	private Disciplina umaDisciplina;
	@Before
		public void setUp()throws Exception{
			umControleDisciplina = new ControleDisciplina();
			umaDisciplina = new Disciplina();
		}
	@Test
	public void testeAdicionarDisciplina() throws Exception {
		
		umaDisciplina.setNomeDisciplina("Calculo 1");		
		umControleDisciplina.adicionarDisciplina(umaDisciplina);		
		umControleDisciplina.pesquisarDisciplina("Calculo 1");
		assertEquals(umControleDisciplina.pesquisarDisciplina("Calculo 1"), umaDisciplina);
		
	}
	
}
