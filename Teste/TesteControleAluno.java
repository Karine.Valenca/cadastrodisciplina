package Teste;


import Model.Aluno;
import Model.ControleAluno;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteControleAluno {
	private ControleAluno umControleAluno;
	private Aluno umAluno;
	@Before
		public void setUp()throws Exception{
			umControleAluno = new ControleAluno();
			umAluno = new Aluno();
		}
	@Test
	public void testeAdicionarAluno() throws Exception {
		
		umAluno.setNome("Lucas");		
		umControleAluno.adicionar(umAluno);		
		umControleAluno.pesquisarAluno("Lucas");
		assertEquals(umControleAluno.pesquisarAluno("Lucas"), umAluno);
	}
	@Test
	public void testeSetGetNomeAluno() {
	umAluno.setNome("Karine");
	assertEquals("Karine", umAluno.getNome());
	}
	@Test
	public void testSetGetMatricula() {
	umAluno.setMatricula("130012050");
	assertEquals("130012050", umAluno.getMatricula());
	}
}
